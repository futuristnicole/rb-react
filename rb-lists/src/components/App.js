import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './organisms/Header';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';
import Level from './pages/Level';
import Lesson from './pages/Lesson';
// import NewLesson from './pages/NewLesson';

class App extends Component {
  render() {
    return (
      <div className="">
        <BrowserRouter>
          <Header />
          <Switch>
            <Route path="/about" exact component={About} />
            <Route path="/contact" exact component={Contact} />
            {/* <Route path="/lesson/new" exact component={NewLesson} /> */}
            <Route path="/level/:levelname" exact component={Level} />
            <Route path="/lesson/:id" exact component={Lesson} />
            <Route path="/" component={Home} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
