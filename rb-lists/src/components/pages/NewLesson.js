import React, { Component } from 'react';
import LessonFetch from '../../queries/LessonFetch';


class NewLesson extends Component {
    constructor(props) {
        super(props);
        this.state = { name: ''};
    }
    onSubmit(event) {
        event.preventDefault();
        this.props.mutate({
            variables: {    name: this.state.name, 
                            description: this.state.description, 
                             
                        },
            refetchQueries: [{ query: LessonFetch }]
        });
        // need to send user back to list after added lesson
    }
    render() {
        return (
            <div>
                <h1>NewLesson Page</h1>
                <h3>Create a New Lesson</h3>
                <form onSubmit={this.onSubmit.bind(this)}>
                    <label>Name of Lesson:</label>
                    <input 
                        onChange={event => this.setState({ name: event.target.value })}
                        value={this.state.name} 
                    />
                </form>
            </div>
        );
    }
};

export default NewLesson;