import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import LessonFetch from '../../queries/LessonFetch';

class Lesson extends Component {
    // renderLesson() {
    //     // const lessonName = this.props.match.params.lessonname
    //     console.log(this.props);  
    //     return (
    //         <div>
    //             <h1>Lesson Page</h1>
    //             {/* <h2>{lessonName}</h2> */}
    //         </div>
    //     );
    // }
    render() {
        // const lessonName = this.props.match.params.lessonname
        const { lesson }  = this.props.data.Lesson;
        // console.log(this.props.data);
        console.log(this.props.data.Lesson);
        if (this.props.data.loading) { return <div>Loading...</div>; } 
        return (
            <div>
                <h1>Lesson Page - </h1>
                <h1>Lesson Page - {lesson.name}</h1>
                {/* {this.renderLesson()} */}
            </div>
        );
    }
};

export default graphql(LessonFetch, {
    options: (props) => { return { variables: { id: props.match.params.id } } }
} )(Lesson);