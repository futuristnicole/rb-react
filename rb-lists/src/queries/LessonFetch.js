import gql from "graphql-tag";

const LessonFetch = gql`
    query LessonFetch($id: ID!) {
        Lesson(id: $id) {
            id
            name
            language
            dialect
            headline
            description
            level
            subject
            topic
        }
    }
`;

export default LessonFetch;